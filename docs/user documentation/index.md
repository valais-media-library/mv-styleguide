# How to use MV-Styleguide

Because MV-Styleguide is framework or environment agnostic, it means that you can use it on any kind of frontend project. Basically, it's only **HTML**, **CSS** and a tiny bit of **JavaScript** like any kind of CSS frameworks.

You have **2 options** :
- You can get the sources and include it on your own frontend infrastructure (advanced users)
- or simply get the build and linked them to your project (recommended)

## Get the sources

Because you're supposed to be an advanced user, you can do whatever you want and include MV-Styleguide as a Git submodule or as a symlink in your project. Then the Sass and javascript’s entry points are in `assets/components/base.[scss, js]`

⚠️ This method can cause some conflict with your own project and the stability cannot be ensured.

## Get the builds (recommended)

Under the [releases tab](https://gitlab.com/valais-media-library/mv-styleguide/releases), you will find all the releases of MV-Styleguide and the related changes.

**To download the last release's build**, you can [download this zip](https://gitlab.com/valais-media-library/mv-styleguide/archive/dist/frontend.zip) which always reflect the last release state of MV-Styleguide. If you feel more comfortable with Github, the branch `dist/frontend` is the build location.

Now that you've downloaded the build archive, you will find every CSS, Javascript and assets files required to apply MV-Styleguide design on your project.

### A recommended way to use MV-Styleguide

1. Unarchive the builds under something like `./vendors/styleguide`
2. In your base HTML template, include the following code
  ```html
  <head>
    <!-- [...] -->

    <!-- MV-Styleguide CSS -->
    <link rel="stylesheet" href="./vendors/styleguide/css/vendors.min.css">
    <link rel="stylesheet" href="./vendors/styleguide/css/base.css">

    <!-- [...] -->
  </head>
  <body>
    <!-- [...] -->

    <!-- jQuery through CDN is not already present -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <!-- MV-Styleguide JS -->
    <script src="./vendors/styleguide/js/vendors.min.js"></script>
    <script src="./vendors/styleguide/js/vendors.bundle.js"></script>
    <script src="./vendors/styleguide/js/app.bundle.js"></script>
  </body>
  ```

## Something is wrong ?

Feel free to use the [support and request page](#/doc/Support & request--Request.md) or to open an issue on the [Github repository](https://gitlab.com/valais-media-library/mv-styleguide/issues).
