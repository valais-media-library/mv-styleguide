/* globals jQuery, document */

// You will use that file to import all your scripts
// Ex: import gallery from './gallery'
import upload from './atoms/upload/upload';
import breadcrumb from './molecules/breadcrumb/breadcrumb';
import datepicker from './molecules/datepicker/datepicker';
import datepickerEvent from './molecules/datepicker/datepicker-fancy';
import popover from './atoms/popover/popover';
import gallery from './molecules/gallery/gallery';
import share from './molecules/share/share';
import selectMultiple from './atoms/select/select-multiple';
import tagInput from './atoms/tag/tag-input';
import cardSlider from './organisms/card-slider/card-slider';
import nav from './organisms/nav-main/nav-main.js';
import drawer from './atoms/drawer/drawer.js';
import search from './molecules/search/search.js';
import cookieconsent, { get_cookieconsent_config } from './organisms/cookie-consent/cookie-consent.js';
// import svgIcons from '../icons/svg-icons';

// svgIcons(); // Must run as soon as possible

const init = () => {
  // Run your imported scripts
  // Ex: gallery();
  upload();
  selectMultiple();
  tagInput();
  datepicker();
  datepickerEvent();
  popover();
  gallery();
  search();
  share();
  Tablesaw.init();
  nav();
  cardSlider();
  drawer();
  breadcrumb();
  cookieconsent(get_cookieconsent_config());
};

(function ($) {
  $(document).ready(() => init());
})(jQuery);
document.addEventListener('ToolboxReady', () => init());
