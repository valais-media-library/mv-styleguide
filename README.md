# MV-Styleguide

Valais Media Library Web Styleguide

#### [👉 View online](https://valais-media-library.gitlab.io/mv-styleguide)

## To start
- **serve** your project : `$ yarn start`
- **build** your project : `$ yarn build`
- **deploy** your gh-pages : `$ yarn deploy`
- **publish** your frontend build : `$ sh ./publish.sh VERSION<0.0.0> ON_NPM<true>`

## Install

To contribute and run the styleguide, you will need few things :

- [🔀 Git](https://git-scm.com/) - Version control system

- either:

  - [📗 NodeJS 8+](https://nodejs.org/en/) - JavaScript runtime used to build the project
  - [🐈 Yarn](https://yarnpkg.com/lang/en/) - Dependency manager built on top of the NPM registry

Then, to install the project on your environment :

```bash
$ git clone git@gitlab.com:valais-media-library/mv-styleguide.git
$ cd ./mv-styleguide
$ yarn
```

## Run locally

As listed in the `package.json` you have **3 commands** available :

- `$ yarn start` : Will launch a live reloaded server to help you **during development**
- `$ yarn build` : Will build your assets for **production usage**
- `$ yarn deploy` : Will **deploy the styleguide** in the dedicated [`gh-pages`](https://epfl-idevelop.github.io/elements)

## Create a new release

#### 1. Git release

This project follows the [git-flow](https://danielkummer.github.io/git-flow-cheatsheet/)'s guidelines. It means you must use the following command to start a new release from your local **`dev`** branch :

```bash
$ git flow release start x.x.x
```

#### 2. Changelog & versions

Because a new release can impact a lot of projects who use *MV-Styleguide*, **you must precisely list\* all the updates made on the components markup** in the **`CHANGELOG.md`**.

**Check previous versions to give you an idea of how to write it the right way*

Then, don't forget to **update the version number** in the `VERSION` and the `package.json` files.

Commit everything !

#### 3. Complete the release

First, you must complete the [git-flow](https://danielkummer.github.io/git-flow-cheatsheet/) release process with the following command :

```bash
$ git flow release finish -p 'x.x.x'
```

Because the previous command will normally push your release commits and tag, simply **go on Github\* and copy/paste the release's changelog content** in the release's description. (go directly using `https://github.com/epfl-idevelop/elements/releases/edit/x.x.x`)

#### 4. Publish the builds

Complete every commit and tag message if needed. Then put yourself in your local **`master`** branch and type the following command to **start the build publishing task** :

```bash
$ sh publish.sh x.x.x
```

That's it ! If everything went fine, **the new release's build is available on the `dist/frontend`** branch thanks to you !

##### 4.1 Note on build images

Not all images in the styleguide are needed in the `dist/frontend` branch. Most of them only serve a presentation purpose and would be a burden for the production builds. For this reason **the WHOLE `/images/styleguide/` FOLDER** is **removed** during publishing, in the `publish.sh` script.

If you want to ship images to the production build, just make shure they are in the `/images/` folder or any subfolder, as long as it is not named `/images/styleguide/`.

All the images in the `/images/styleguide/` folder will remain available when running `yarn start` or deploying the styleguide on a gh-page.

#### 5. Update github.io

- A `$ yarn deploy` should **deploy the styleguide** in the dedicated [`gh-pages`](https://epfl-idevelop.github.io/elements)

## Acknowledgements

**Inspiration**: The project is inspired form the [EPFL Web Styleguide]([https://github.com/epfl-idevelop/elements](https://github.com/epfl-idevelop/elements) (epfl-element) projects. Project under the [MIT license](https://github.com/epfl-idevelop/elements/blob/master/package.json).

**Generator**: The project is generated with the [generator-toolbox](https://github.com/frontend/generator-toolbox) developped by [Antistatique](https://antistatique.net/). Project under the [MIT license](https://github.com/frontend/generator-toolbox/blob/843e107a3f3834d98c8fa95a1590f47889ba1196/README.md#license).
